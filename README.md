

														======================
														   HRM Interview BOT
														=======================

														
								/**
								 * @license Copyright (c) 2018, ERA-INFOTECH LIMITED. All rights reserved.
								 */											


								 
# What is HRM Interview BOT ? 
===============================

HRM Interview is a NLP based service which stimulates HCI (Human Computer Interaction) in a way that canidates applied for a certain
vacancy (Example: Software Engineer) can pefrom a basic interview with the BOT. In other words, ChatBot could simulate a interview
with a registered candidate on befalf of HR. And the results of interview will be saved in Databases.

HRM Interview BOT has been intregated with the e-Recruitment Software developed by ERA-INFOTECH LIMITED. So, after successfully applying
for a specific post in the e-Recruitment service candiates could make a feel of performing interview thourgh this ChatBOT.


# Example:
===========
 
	Assume, a dummy candiate has been applied for the post "Assistant Software Engineer". Then the candiate could perform a interview session
	with a valid Application/ Tracking ID.
	
	BOT: Welcome Mr.X! You have been applied for the post "Assistant Software Engineer". I'm a initiating a short interview session for you.
		 Are you Ready ? 
		 
	Candidate: Yes. I'm Ready!
	
	BOT: Great! From which university you have been graduated ? 
	
	Candidate: University X!
	
	BOT: What is JVM ? 
	
	Candiate: JVM is java virutal machine for compiling and execuint java programs.
	.......
	

After successfull interview session the complete collection of Questions and Answers will be recorded for per candiate possessing an unique
Application/Tracking ID. So, Examiner could grade the candiate's interview sessing later!! 
	
	

# Features:
==========

	1. Extended Database interaction capability.
	2. Fully supported with Python Request-Response mechanism.
	3. Could respond to multiple intents based on action parameters.
	4. Able to receive request parameters as JSON and prepare response for user intelligently. 
	5. Extract parametric values from user to generate sql query using different algoritms independently. 
	6. Nested requested could be invoked to interact external webservices as well as RESTFUL Web Services.
	7. User Authentication support added.
	8. SSL (Secured Socket Layer) included.
	
	
# Regards: 
==========

Developer : Salman Rakin
Project Manager: Anwar Hossain

Artificial Intelligence Team
ERA-INFOTECH LIMITED.
	