/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.era;

import com.connect.DBConnectionHandler;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.wink.json4j.JSONException;
import org.apache.wink.json4j.OrderedJSONObject;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Salman Rakin
 */
@WebServlet(name = "ApplicationStatus", urlPatterns = {"/ApplicationStatus"})
public class ApplicationStatus extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ApplicationStatus</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ApplicationStatus at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        //application_id="0"+application_id;
        
        //contact_no="0"+contact_no;
        String action= request.getParameter("act");
        String format = request.getParameter("format");
        String sql = null;
        String sql_insert = null;
        String result=null;
        String flag=null;
        String app_id="";
        
        out.print(action);
        out.print(app_id);
        
        //String query="SELECT DISTINCT appl_status_desc FROM ocasmn.vw_appl_sts_info WHERE application_id = 'DE16030144'";
        
        
        
        //out.println(query_procedure); 
        //String[] res= new String[500];
        
        ArrayList<String> res= new ArrayList<String>(); 
        
        //for(int i=0;i<60;i++) res[i]=null;
        
        //String[] res={};
        
        Statement stmt = null;
        Connection con = DBConnectionHandler.getConnection();
        
        
        try {
                    
            if(action.equals("Application.ID")){
                
                String application_id=request.getParameter("p").trim();
                application_id="0"+application_id;
                out.println(application_id);
                sql="SELECT FULL_NAME, ANS_STATUS FROM APL_INFO WHERE MOBILE_NO='"+application_id+"'";
                
                                            
            }  
            else if(action.equals("Contact.No")){
                
                String contact_no=request.getParameter("p").trim();
                contact_no="0"+contact_no;
                out.println(contact_no);
                sql="SELECT FULL_NAME, ANS_STATUS FROM APL_INFO WHERE MOBILE_NO='"+contact_no+"'";
                                            
            }
            else if(action.equals("Q.1")){
                
                app_id=request.getParameter("p").trim();
//                out.println(app_id);
//                Float app_id_f=Float.parseFloat(app_id);
//                Integer app_id_int=Math.round(app_id_f);
//                app_id=app_id_int.toString();
                app_id="0"+app_id;
                out.println(app_id);
                
                CallableStatement statement = con.prepareCall("{call DPR_BOT_QUES_SET(?, ?, ?, ?)}");
                
                statement.setString(1, "200");
                statement.setString(2, app_id);
                
                statement.registerOutParameter(3, Types.VARCHAR);
                statement.registerOutParameter(4, Types.VARCHAR);
                
                statement.execute();
                
                String messge_flag= statement.getString(3).trim();
                String messge_desc= statement.getString(4).trim();
                
                
                out.println("Procedure Message flag: "+messge_flag);
                out.println("Procedure Message desc: "+messge_desc);
                
                statement.close();
                
                
                
                sql="SELECT QUESTION_ID, QUESTION_DESC,NVL(QUES_OPTION,' ') FROM (SELECT * FROM VW_BOT_QUESTION ORDER BY ANSWER_TYPE DESC) VW_BOT_QUESTION WHERE USER_ID='"+app_id+"' AND NVL(IS_ANSWER,'N')='N' AND ROWNUM=1";
            
            
            }
            
            else if(action.equals("Q.2")){
                
                app_id=request.getParameter("p").trim();
                app_id=app_id.substring(0, app_id.length() - 2);
                app_id="0"+app_id;
                out.println(app_id);
                
                String question_id=request.getParameter("Question_ID").trim();
                String answer= request.getParameter("Answer").trim();
                
                stmt=con.createStatement();
                
                sql_insert="UPDATE VW_BOT_QUESTION SET IS_ANSWER='Y' WHERE USER_ID='"+app_id+"' AND QUESTION_ID='"+question_id+"'";
                //out.println(sql_insert);
                out.println(stmt.executeUpdate(sql_insert));
                
                sql_insert="INSERT INTO RCBOT_Q_ANS (COMPCDE, USER_CODE, Q_CODE, Q_POSTCDE, Q_ANSWER, CREATEDBT,CREATEDDT) VALUES ('200', '"+app_id+"', '"+question_id+"', 'SE(APEX)', '"+answer+"', 'BOT',SYSDATE)";
                
                out.println(stmt.executeUpdate(sql_insert));
                
                sql="SELECT QUESTION_ID, QUESTION_DESC,NVL(QUES_OPTION,' ') FROM (SELECT * FROM VW_BOT_QUESTION ORDER BY ANSWER_TYPE DESC) VW_BOT_QUESTION WHERE USER_ID='"+app_id+"' AND NVL(IS_ANSWER,'N')='N' AND ROWNUM=1";
            
            
            }
            
            else if(action.equals("Interview.Session")){
                
                String contact_info=request.getParameter("p").trim();
                contact_info="0"+contact_info;
                out.println(contact_info);
                String ans1=request.getParameter("a1").trim();
                String ans2=request.getParameter("a2").trim();
                String ans3=request.getParameter("a3").trim();
                String ans4=request.getParameter("a4").trim();
                
                stmt=con.createStatement();
                
                sql_insert="INSERT INTO RCBOT_Q_ANS(COMPCDE, USER_CODE, Q_CODE, Q_POSTCDE,Q_ANSWER, CREATEDDT)VALUES('200','"+contact_info+"','1','SE','"+ans1+"',SYSDATE)";
                //out.println(sql_insert);
                out.println(stmt.executeUpdate(sql_insert));
                
                sql_insert="INSERT INTO RCBOT_Q_ANS(COMPCDE, USER_CODE, Q_CODE, Q_POSTCDE,Q_ANSWER, CREATEDDT)VALUES('200','"+contact_info+"','2','SE','"+ans2+"',SYSDATE)";
                //out.println(sql_insert);
                out.println(stmt.executeUpdate(sql_insert));
                
                sql_insert="INSERT INTO RCBOT_Q_ANS(COMPCDE, USER_CODE, Q_CODE, Q_POSTCDE,Q_ANSWER, CREATEDDT)VALUES('200','"+contact_info+"','4','SE','"+ans3+"',SYSDATE)";
                //out.println(sql_insert);
                out.println(stmt.executeUpdate(sql_insert));
                
                sql_insert="INSERT INTO RCBOT_Q_ANS(COMPCDE, USER_CODE, Q_CODE, Q_POSTCDE,Q_ANSWER, CREATEDDT)VALUES('200','"+contact_info+"','3','SE','"+ans4+"',SYSDATE)";
                //out.println(sql_insert);
                out.println(stmt.executeUpdate(sql_insert));
                
            }
            
            
            
                out.println(sql);
                out.println(action);
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                OrderedJSONObject json= new OrderedJSONObject();

             if(sql !=null && sql !=""){       
                PreparedStatement ps = con.prepareStatement(sql);
                //ps.executeUpdate();
                ResultSet rs = ps.executeQuery();


                while(rs.next()){ 

                    if(action.equals("Application.ID")){

                        result = rs.getString("FULL_NAME");
                        flag= rs.getString("ANS_STATUS");
                        out.println(flag);
                        map.put("result", result);
                        map.put("flag",flag);
                        json.put("Status", map);
                        out.println(json);
                        

                    }

                    else if(action.equals("Contact.No")){

                        result = rs.getString("FULL_NAME");
                        flag= rs.getString("ANS_STATUS");
                        out.println(flag);
                        map.put("result", result);
                        map.put("flag",flag);
                        json.put("Status", map);
                        out.println(json);

                    }
                    
                    else if(action.equals("Q.1") || action.equals("Q.2") ){
                        
                        OrderedJSONObject json_temp = new OrderedJSONObject();
                        
                        String question_id= rs.getString(1);
                        String question_desc=rs.getString(2);
                        String options=rs.getString(3);
                        json_temp.put("Question ID", question_id);
                        json_temp.put("Question Desc", question_desc);
                        json_temp.put("Options", options);
                        json.put("App_ID",app_id);
                        json.put("Question_Details",json_temp);
                        
                        
                    }
                    

                }

             }
                //LinkedHashMap<String, Map<String,Object>> map_json= new LinkedHashMap<String, Map<String,Object>>();
            
            
            
            
            
            
            //JSONObject json = new JSONObject();
            
//            list.add(map);
//            json.put("Status", list);
            
            
            
            /*
            
            if (action.equals("Performance.individual")){ // watch closely 
                
                
                JSONObject orderedJson= new JSONObject(map_json);
                JSONArray jsonArray = new JSONArray();
                jsonArray.add(Arrays.asList(orderedJson));
              
            
            
                //JSONObject json= new JSONObject();
                json.put("Number of Rows",row_count-1);
                json.put("Query",jsonArray);
            
                
            
            }
                
            */
            
            response.addHeader("Access-Control-Allow-Origin", "*");
            response.setCharacterEncoding("UTF-8");
            
            if("json".equals(format)){
                response.setContentType("application/json");
                response.getWriter().print(json.toString());
            }
            else{
                 response.setContentType("text/plain");
                response.getWriter().print(result);
            }
            response.getWriter().flush();
            
        } catch (SQLException ex) {
            Logger.getLogger(ApplicationStatus.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(ApplicationStatus.class.getName()).log(Level.SEVERE, null, ex);
        }
        DBConnectionHandler.releaseConnection(con);
          
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
